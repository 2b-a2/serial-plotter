#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <stdexcept>
#include <QDebug>
#include <QSerialPortInfo>
#include <QInputDialog>
#include <QMessageBox>
#include <iostream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    dataQ_(windowSize, 0)
{
    ui->setupUi(this);

    // get the user to select a serial port
    const auto ports = QSerialPortInfo::availablePorts();
    QStringList portNames;
    for (const auto& port: ports)
    {
        portNames.push_back(port.portName());
    }
    QString portName = QInputDialog::getItem(this, "Serial port selection", "Select a serial port:", portNames, 0, false);
    serialPort_.setPortName(portName);
    bool ok = false;
    while (!ok)
    {
        QString baudRate = QInputDialog::getText(this, "Baud rate selection", "Enter baud rate:");
        serialPort_.setBaudRate(baudRate.toInt(&ok));
    }

    connect(&serialPort_, &QSerialPort::readyRead, this, &MainWindow::readyRead);

    const bool success = serialPort_.open(QIODevice::ReadOnly);
    if (!success)
    {
        QMessageBox msg;
        msg.setText(QString("Failed to open serial port: %1").arg(serialPort_.errorString()));
        msg.exec();
        throw std::runtime_error("Failed to open serial port");
    }

    // set up the plot
    QCustomPlot* plot = ui->plot;
    plot->yAxis->setRange(0.0, 5000.0);
    valueGraph_ = plot->addGraph();
    valueGraph_->setPen(QPen(Qt::red));
    integralGraph_ = plot->addGraph();
    integralGraph_->setPen(QPen(Qt::green));
    derivativeGraph_ = plot->addGraph();
    derivativeGraph_->setPen(QPen(Qt::blue));
    logarithmGraph_ = plot->addGraph();
    logarithmGraph_ ->setPen(QPen(Qt::yellow));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readyRead()
{
    // there is new data from the serial port
    QCustomPlot* plot = ui->plot;
    const auto newBytes = serialPort_.readAll();
    currentData_.append(newBytes);
    auto splitData = currentData_.split('\n');
    currentData_ = splitData.last();
    splitData.removeLast();
    for (const auto& data: splitData)
    {
        bool ok = false;
        int di = data.toInt(&ok);
        if (ok)
        {
            //const int back = dataQ_.back();
            dataQ_.push_back(di);
            //const int oldSum = sum_;
            sum_ += di;
            const int front = dataQ_.front();
            sum_ -= front;
            dataQ_.pop_front();
            singleSum_ = 0.6 * singleSum_ + di;

            const float integral = sum_ / windowSize;
            const int derivative = dataQ_.back() - front;
            const float loga = log(di);


            //PreHit
            if ((integral == 0) && (derivative == 0) && (preHit_ == false) && (startHit_ == false) &&(endHit_ == true)) {
                ++count_;
                std::cout << "Pre HIT at step " << step_ << " count " << count_ << std::endl;
                endHit_ = false;
                preHit_ = true;
            }

            //Hit
            if ((integral < derivative ) && (preHit_ == true) && (startHit_ == false) && (endHit_ == false)) {
                std::cout << "HIT at step" << step_ << " count " << count_ << std::endl;
                preHit_ = false;
                startHit_ = true;
            }

            /*
            //Remove noise if it is actually a noise
            if ((integral > derivative * 2 ) && (derivative < 1000) && (preHit_ == false) && (startHit_ == true) && (endHit_ == false)) {
                --count_;
                std::cout << "NOISE!!" << step_ << " count back to " << count_ << std::endl;
                startHit_ = false;
                endHit_ = true;
            }
            */

            //EndHit
            if ((integral > derivative ) && (derivative < 100) && (preHit_ == false) && (startHit_ == true) && (endHit_ == false)) {
                if (countEnd_ == 50){
                    std::cout << "End HIT at step" << step_ << " count " << count_ << std::endl;
                    startHit_ = false;
                    endHit_ = true;
                    countEnd_= 0;
                } else {
                    countEnd_ += 1;
                }
            }

            /* Two points hit detection
             *
            if ((integral == 0) && (derivative == 0) && (endHit_ == true) && (startHit_ == false)) {
                ++count_;
                std::cout << "HIT at step " << step_ << " count " << count_ << std::endl;
                startHit_ = true;
                endHit_ = false;
            }

            if ((integral < derivative ) && (startHit_ == true) && (endHit_ == false)) {
                std::cout << "End HIT at step" << step_ << " count " << count_ << std::endl;
                startHit_ = false;
                endHit_ = true;
            }
            */

            integralGraph_->addData(step_, integral);
            //integralGraph_->addData(step_, singleSum_);
            //valueGraph_->addData(step_, di);
            // Two derivativeGraph_ functions return the same graph
            derivativeGraph_->addData(step_, (dataQ_.back() - front));
            //derivativeGraph_->addData(step_, (sum_ - oldSum));
            //logarithmGraph_->addData(step_, loga * 100);
            ++step_;
        }
    }
    plot->xAxis->setRange(static_cast<double>(step_), 10000.0, Qt::AlignRight);
    plot->replot();
}
