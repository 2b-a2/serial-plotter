#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSerialPort>
#include "qcustomplot.h"
#include <deque>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void readyRead();
    int step_ = 0;
    Ui::MainWindow *ui;
    QCPGraph* valueGraph_;
    QCPGraph* integralGraph_;
    QCPGraph* derivativeGraph_;
    QCPGraph* logarithmGraph_;
    QString currentData_;
    QSerialPort serialPort_;
    std::deque<int> dataQ_;
    int sum_ = 0;
    bool preHit_ = false;
    bool startHit_ = false;
    bool endHit_ = true;
    int count_ =0;
    int singleSum_ = 0;
    int countEnd_ = 0;

    static const int windowSize = 10;
};

#endif // MAINWINDOW_H
